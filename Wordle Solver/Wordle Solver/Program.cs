﻿using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

/* This program is ment to solve the 5 letter word game WORDLE */
/* This point is to demonstrate skills such as loops, file IO, */
/* iteration, using/modifying containers,  */





namespace testing
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Declare all variables */
            string fileIn = System.IO.File.ReadAllText(@"C:\Users\jeschete\Documents\wordlist.txt");
            string[] words = fileIn.Split(' ', '\n');
            string letterChoice;
            ArrayList wordArr = loadWords(words);
            ArrayList lettersUsed = new ArrayList();
            Boolean runCont = true;
            int menuChoice = 0;
            int letterChoiceCheck = 0;
            int numberChoice = 0;
            string numParse;

            /* Main Program Loop */
            while (runCont == true)
            {
                switch (menuChoice)
                {
                    /* Main Menu */
                    case 0:
                        Console.WriteLine("");
                        Console.WriteLine("Words in Remaining: " + wordArr.Count);
                        Console.WriteLine("What do you want to do?");
                        Console.WriteLine("1. Eliminate every word containing a specific letter?");
                        Console.WriteLine("2. Eliminate words based on a letter in a certain position.");
                        Console.WriteLine("3. Eliinate words that do not contain given letter.");
                        Console.WriteLine("4. Eliminate words that do not have a letter in a certain position.");
                        Console.WriteLine("5. Print word list.");
                        Console.WriteLine("6. Quit.");
                        Console.WriteLine("Please enter choice as a number 1-6.");
                        bool success = int.TryParse(Console.ReadLine(), out menuChoice);
                        if (success == false)
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Please enter a valid choice. ");
                            Console.WriteLine("");
                        }
                        Console.WriteLine("");
                        break;

                    /* Remove all words containing inputed letter. */
                    case 1:
                        Console.WriteLine("");
                        Console.WriteLine("Words in Remaining: " + wordArr.Count);
                        Console.WriteLine("Please enter a single letter A-Z, or type quit to return to main menu. ");
                        Console.WriteLine("");
                        printUsedLetters(lettersUsed);
                        letterChoice = Console.ReadLine().ToUpper();
                        success = int.TryParse(letterChoice, out letterChoiceCheck);
                        if ((success == true) | (letterChoice.Length > 1))
                        {
                            if (letterChoice == "QUIT")
                            {
                                menuChoice = 0;
                                break;
                            }
                            Console.WriteLine("");
                            Console.WriteLine("Please only enter a single letter. ");
                        }
                        else
                        {
                            if (lettersUsed.Contains(letterChoice) == false)
                            {
                                lettersUsed.Add(letterChoice);
                            }

                            Console.WriteLine("");
                            Console.WriteLine("Deleting Entries Containing the letter " + letterChoice);
                            Thread.Sleep(1000);
                            ArrayList localArr = new ArrayList(wordArr);
                            foreach (string str in wordArr)
                            {
                                for (int i = 0; i < 5; i++)
                                {
                                    if (letterChoice.ToString() == str[i].ToString())
                                    {
                                        localArr.Remove(str);
                                    }
                                }
                            }
                            wordArr = localArr;
                        }
                        Console.WriteLine("Words in Remaining: " + wordArr.Count);
                        Console.WriteLine("");
                        break;

                    /* Remove all words based on given numbered position and letter */
                    case 2:
                        Console.WriteLine("");
                        Console.WriteLine("Words in Remaining: " + wordArr.Count);
                        printUsedLetters(lettersUsed);
                        Console.WriteLine("Please Choose the spot to check between 1 and 5. ");
                        numParse = Console.ReadLine();
                        Console.WriteLine("");
                        Console.WriteLine("Please enter a single letter A-Z, or type quit to return to main menu. ");
                        letterChoice = Console.ReadLine().ToUpper();
                        success = int.TryParse(numParse, out numberChoice);
                        bool success2 = int.TryParse(letterChoice, out letterChoiceCheck);
                        if ((success == true) & (numberChoice <= 5))
                        {
                            if ((success2 == true) | (letterChoice.Length > 1))
                            {
                                if (letterChoice == "QUIT")
                                {
                                    menuChoice = 0;
                                    break;
                                }
                                Console.WriteLine("");
                                Console.WriteLine("Please only enter a single letter. ");
                            }
                            else
                            {
                                if (lettersUsed.Contains(letterChoice) == false)
                                {
                                    lettersUsed.Add(letterChoice);
                                }

                                Console.WriteLine("");
                                Console.WriteLine("Deleting Entries Containing the letter " + letterChoice + " in position " + numberChoice);
                                Thread.Sleep(1000);
                                ArrayList localArr = new ArrayList(wordArr);
                                foreach (string str in wordArr)
                                {
                                    Boolean comparison = (letterChoice.ToString() == str[numberChoice - 1].ToString());
                                    if (letterChoice.ToString() == str[numberChoice - 1].ToString())
                                    {

                                        Console.WriteLine("Comparing: " + letterChoice.ToString() + " and " + str[numberChoice - 1].ToString());
                                        Console.WriteLine("Result: " + comparison);
                                        localArr.Remove(str);
                                    }
                                }
                                wordArr = localArr;
                            }
                        }
                        else
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Please only enter number between 1 and 5. ");
                        }
                        Console.WriteLine("Words in Remaining: " + wordArr.Count);
                        Console.WriteLine("");
                        break;

                    /* Remove all words not containing given letter. */
                    case 3:
                        Console.WriteLine("");
                        Console.WriteLine("Words in Remaining: " + wordArr.Count);
                        Console.WriteLine("Please enter a single letter A-Z, or type quit to return to main menu. ");
                        Console.WriteLine("");
                        printUsedLetters(lettersUsed);
                        letterChoice = Console.ReadLine().ToUpper();
                        success = int.TryParse(letterChoice, out letterChoiceCheck);
                        if ((success == true) | (letterChoice.Length > 1))
                        {
                            if (letterChoice == "QUIT")
                            {
                                menuChoice = 0;
                                break;
                            }
                            Console.WriteLine("");
                            Console.WriteLine("Please only enter a single letter. ");
                        }
                        else
                        {
                            if (lettersUsed.Contains(letterChoice) == false)
                            {
                                lettersUsed.Add(letterChoice);
                            }

                            Console.WriteLine("");
                            Console.WriteLine("Deleting Entries that do not contain the letter: " + letterChoice);
                            Thread.Sleep(1000);
                            ArrayList localArr = new ArrayList(wordArr);
                            foreach (string str in wordArr)
                            {
                                if (str.Contains(letterChoice) == false)
                                {
                                    localArr.Remove(str);
                                }
                            }
                            wordArr = localArr;
                        }
                        Console.WriteLine("Words in Remaining: " + wordArr.Count);
                        Console.WriteLine("");
                        break;

                    /* Delete words that do have specified letter in a certain spot.*/
                    case 4:
                        Console.WriteLine("");
                        Console.WriteLine("Words in Remaining: " + wordArr.Count);
                        printUsedLetters(lettersUsed);
                        Console.WriteLine("Please Choose the spot to check between 1 and 5. ");
                        numParse = Console.ReadLine();
                        Console.WriteLine("");
                        Console.WriteLine("Please enter a single letter A-Z, or type quit to return to main menu. ");
                        letterChoice = Console.ReadLine().ToUpper();
                        success = int.TryParse(numParse, out numberChoice);
                        success2 = int.TryParse(letterChoice, out letterChoiceCheck);
                        if ((success == true) & (numberChoice <= 5))
                        {
                            if ((success2 == true) | (letterChoice.Length > 1))
                            {
                                if (letterChoice == "QUIT")
                                {
                                    menuChoice = 0;
                                    break;
                                }
                                Console.WriteLine("");
                                Console.WriteLine("Please only enter a single letter. ");
                                Console.WriteLine("");
                            }
                            else
                            {
                                if (lettersUsed.Contains(letterChoice) == false)
                                {
                                    lettersUsed.Add(letterChoice);
                                }

                                Console.WriteLine("");
                                Console.WriteLine("Deleting Entries that do not contain the letter " + letterChoice + " in position " + numberChoice);
                                Thread.Sleep(1000);
                                ArrayList localArr = new ArrayList(wordArr);
                                foreach (string str in wordArr)
                                {
                                    if (letterChoice.ToString() != str[numberChoice - 1].ToString())
                                    {
                                        localArr.Remove(str);
                                    }
                                }
                                wordArr = localArr;
                            }
                        }
                        else
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Please only enter number between 1 and 5. ");
                            Console.WriteLine("");
                        }
                        Console.WriteLine("");
                        Console.WriteLine("Words in Remaining: " + wordArr.Count);
                        Console.WriteLine("");
                        break;

                    /* Print word list */
                    case 5:
                        Console.WriteLine("Writing words to list.");
                        for (int i = 0; i < wordArr.Count; i++)
                        {
                            WriteArrayListToFile(wordArr);
                        }
                        System.Diagnostics.Process.Start(@"C:\Users\jeschete\Documents\NewWordList.txt");
                        menuChoice = 0;
                        break;

                    /* End Program. */
                    case 6:
                        Console.WriteLine("Thank you, I hope you found your word! ");
                        for (int i = 0; i < 5; i++)
                        {
                            Console.WriteLine("Closing in " + (5 - i));
                            Thread.Sleep(1000);
                        }
                        runCont = false;
                        break;
                    default:

                        Console.WriteLine("Please enter a valid choice. ");
                        Console.WriteLine("");
                        menuChoice = 0;
                        break;
                }
            }
        }




        static ArrayList loadWords(string[] lines)
        {
            ArrayList wordArray = new ArrayList();
            foreach (string line in lines)
            {
                string lineUpper = line.ToUpper();
                wordArray.Add(lineUpper);
            }

            return wordArray;
        }

        public static async Task WriteArrayListToFile(ArrayList ALin)
        {
            File.WriteAllLines(@"C:\Users\jeschete\Documents\NewWordList.txt", ALin.Cast<string>().ToArray());
        }

        static void printUsedLetters(ArrayList letters)
        {
            Console.WriteLine("The used letters are: ");
            Console.Write("[");
            foreach (string str in letters)
            {
                Console.Write(str + ", ");
            }
            Console.WriteLine("]");
        }
    }
}



